package com.example.wagnerwol.wcalc;

import android.content.Intent;
import android.os.Bundle;
import android.renderscript.Double2;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText Num1,Num2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Num1 = (EditText)findViewById(R.id.num1);
        Num2 = (EditText)findViewById(R.id.num2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showResultado(String resultado){
        Intent navegar = new Intent(MainActivity.this,Main2Activity.class);
        navegar.putExtra("resposta", resultado.toString());
        startActivity(navegar);
    }
    public void soma (View v){
        Double n1,n2,res;

        n1 = Double.parseDouble(Num1.getText().toString());
        n2 = Double.parseDouble(Num2.getText().toString());
        res = n1+n2;
        showResultado(res.toString());

    }

    public void subtracao(View v ){
        Double n1,n2,res;
        n1 = Double.parseDouble(Num1.getText().toString());
        n2 = Double.parseDouble(Num2.getText().toString());
        res = n1-n2;
        showResultado(res.toString());
    }

    public void multiplicacao(View v){
        Double n1,n2,res;
        n1 = Double.parseDouble(Num1.getText().toString());
        n2 = Double.parseDouble(Num2.getText().toString());
        res = n1*n2;
        showResultado(res.toString());
    }

    public void divisao(View v){
        Double n1,n2,res;
        n1 = Double.parseDouble(Num1.getText().toString());
        n2 = Double.parseDouble(Num2.getText().toString());
        try{
            res = n1/n2;
            showResultado(res.toString());
        }
        catch(Exception erro ){
            Toast.makeText(MainActivity.this,"Operação Inválida", Toast.LENGTH_LONG).show();
        }

    }


}
